Guile Bindings for Gumbo
========================

**Gumbo was deprecated at 2023-02-15, with the recommendation to not use**

Guile bindings for the
[Gumbo HTML5 parser](https://github.com/google/gumbo-parser/).

Contains the Guile module `(sxml gumbo)`, which exports the single method
`html->sxml`.

Example:

	(html->sxml "Hello")
	⇒ (*TOP* (html (head) (body "Hello")))
	(html->sxml "<!doctype html><title>Minimum valid document</title>")
	⇒ (*TOP* (doctype html) (html (head (title "Minimum valid document")) (body)))

Building
--------

    make
    make install DESTDIR=/usr/local

Testing
-------

    make check
