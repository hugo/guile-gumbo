(define-module (sxml gumbo)
  #:export (html->sxml))

(load-extension "libguile-gumbo" "scm_init_gumbo")

(define* (html->sxml str
                     #:key
                     (trim-whitespace? #t)
                     (full-document? #t))
         (parse-html str
                     `((trim-whitespace? . ,trim-whitespace?)
                       (full-document? . ,full-document?))))
