(use-modules (srfi srfi-64)
             (sxml gumbo))

(test-begin "Gumbo HTML test")

(test-equal "Simple string"
	'(*TOP* (html (head) (body "Hello")))
	(html->sxml "Hello"))

(test-equal "Minimum document"
  '(*TOP* (doctype html) (html (head (title "Minimum valid document")) (body)))
  (html->sxml "<!doctype html><title>Minimum valid document</title>"))

(test-equal "HTML non-nesting tags"
  '(*TOP* (html (head) (body (p) (p))))
  (html->sxml "<p><p>"))

(test-equal "Trim whitespace"
  '(*TOP* (html (head) (body (p) (p))))
  (html->sxml "<p></p>       <p></p>"
              #:trim-whitespace? #t))

(test-equal "Keep whitespace"
  '(*TOP* (html (head) (body (p) "       " (p))))
  (html->sxml "<p></p>       <p></p>"
              #:trim-whitespace? #f))

(test-equal "Trimmed whitespace should keep within string"
  '(*TOP* (html (head) (body (div (p "Hello      World")))))
  (html->sxml "<div>    <p>Hello      World</p>    </div>"))

(test-equal "Full document"
  '(*TOP* (doctype html) (html (head) (body "Hello")))
  (html->sxml "<!doctype HTML>Hello"
              #:full-document? #t))

(test-equal "HTML only"
  '(html (head) (body "Hello"))
  (html->sxml "<!doctype HTML>Hello"
              #:full-document? #f))

(test-equal "Unknown tag"
  '(html (head) (body (unknown-tag)))
  (html->sxml "<unknown-tag/>"
              #:full-document? #f))

(test-equal "unknown tag upper case"
  '(html (head) (body (unknown-tag)))
  (html->sxml "<UNKNOWN-tag/>"
              #:full-document? #f))

(test-equal "unknown tag weird characters"
  '(html (head) (body (unknown-täg)))
  (html->sxml "<unknown-TÄG/>"
              #:full-document? #f))

;;; This test segfaults...
;; (test-equal "HTML4 document"
;;   ""
;;   (html->sxml
;;    "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"
;;             \"http://www.w3.org/TR/html4/strict.dtd\">
;;     <html>
;;     <head>
;;     ...
;;     </head>
;;     <body>
;;     ...
;;     </body>
;;     </html>
;; "
;;    #:full-document? #t)
;;   )

(define failure-count (test-runner-fail-count (test-runner-current)))
(test-end "Gumbo HTML test")

(exit failure-count)
