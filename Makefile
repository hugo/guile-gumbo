.PHONY: all install clean check

CFLAGS=-Wall -pedantic \
	   -std=gnu18 \
	   $(shell pkg-config --cflags gumbo) \
	   $(shell guile-config compile)
LDLIBS=$(shell pkg-config --libs gumbo) \
	   $(shell guile-config link)

GUILE_SITE_DIR=$(shell guile -c "(display (%site-dir))")
GUILE_CCACHE_DIR=$(shell guile -c "(display (%site-ccache-dir))")

SCM_FILES=$(shell find scm -type f -name \*.scm)
GO_FILES=$(SCM_FILES:scm/%.scm=ccache/%.go)

INFOFLAGS=

TARGET=libguile-gumbo.so
DESTDIR=

all: ${TARGET} ${GO_FILES} guile-gumbo.info

guile-gumbo.o: guile-gumbo.x

%.o: %.c
	$(CC) -O2 -c $(CFLAGS) -o $@ -fPIC $<

%.x: %.c
	guile-snarf -o $@ $(CFLAGS) $<

ccache/%.go: scm/%.scm
	@mkdir -p ccache
	guild compile -o $@ $<

${TARGET}: guile-gumbo.o
	$(CC) -shared -o $@ -fPIC $^ $(LDLIBS)

%.info: %.texi
	$(MAKEINFO) -o $@ $(INFOFLAGS) $<

install: all
	mkdir -p $(DESTDIR)$(GUILE_SITE_DIR)
	mkdir -p $(DESTDIR)$(GUILE_CCACHE_DIR)
	cp -ra --no-preserve=ownership scm/* $(DESTDIR)$(GUILE_SITE_DIR)
	cp -ra --no-preserve=ownership ccache/* $(DESTDIR)$(GUILE_CCACHE_DIR)
	install -Dt ${DESTDIR}/usr/lib/ libguile-gumbo.so
	install -m644 -Dt ${DESTDIR}/usr/share/info/ guile-gumbo.info

test_env=LD_LIBRARY_PATH=$$PWD GUILE_LOAD_PATH=$$PWD/scm GUILE_LOAD_COMPILED_PATH=$$PWD/ccache

check: all
	env ${test_env} guile -s tests.scm

clean:
	-rm *.o
	-rm *.so
	-rm *.x
	-rm $(GO_FILES)
