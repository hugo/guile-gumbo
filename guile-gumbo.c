#include <gumbo.h>
#include <libguile.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

typedef struct options {
	bool trim_whitespace;
	bool full_document; /* should the *TOP* and doctype nodes be included? */
} options;

/*
 * Parse the html fragment from the GumboNode into an sxml tree.
 */
static SCM parse_tree (GumboNode*, options*);
/*
 * Convert a single GumboElement to an sxml element.
 * Children are looped through.
 */
static SCM gumbo_element_to_sxml (GumboElement* el, options*);
/*
 * parse each children.
 */
static SCM scm_parse_children (GumboVector* /* GumboNode*/, options*);

#define GUMBO_FOR_EACH(type, node, vector, body) \
	for (int i = 0; i < (vector)->length; i++) { \
		type* node = (type*) vector->data[i]; \
		body; \
	}

SCM_DEFINE_PUBLIC (scm_parse_html, "parse-html", 2, 0, 0,
		(SCM string, SCM options_assq),
		"")
#define FUNC_NAME s_scm_parse_html
{

	char* str = scm_to_utf8_stringn (string, NULL);

	GumboOutput* output = gumbo_parse(str);

	options opts = {
		.trim_whitespace = scm_to_bool(scm_assq_ref(options_assq, scm_from_utf8_symbol("trim-whitespace?"))),
		.full_document = scm_to_bool(scm_assq_ref(options_assq, scm_from_utf8_symbol("full-document?"))),
	};

	SCM retval;
	if (opts.full_document) {
		retval = parse_tree (output->document, &opts);
	} else {
		retval = parse_tree (output->root, &opts);
	}

	gumbo_destroy_output(&kGumboDefaultOptions, output);

	return retval;
}
#undef FUNC_NAME

SCM scm_parse_children (GumboVector* /* GumboNode */ children, options* opts) {
	SCM childvec =
		scm_c_make_vector (
				children->length,
				SCM_UNSPECIFIED);

	GUMBO_FOR_EACH(GumboNode, node, children,
		scm_c_vector_set_x(
				childvec, i, parse_tree(node, opts)));

	return scm_delq (SCM_BOOL_F, scm_vector_to_list (childvec));

}

SCM gumbo_element_to_sxml (GumboElement* el, options* opts) {
	SCM tag;
	if (el->tag < GUMBO_TAG_UNKNOWN) {
		tag = scm_from_utf8_symbol(gumbo_normalized_tagname(el->tag));
	} else {
		/* Unknown tag */
		GumboStringPiece strp = el->original_tag;
		gumbo_tag_from_original_text(&strp);
		// assert strp->length > 0 ???
		SCM str = scm_from_utf8_stringn(strp.data, strp.length);
		str = scm_string_downcase(str);
		tag = scm_string_to_symbol(str);
	}
	SCM scm_children = scm_parse_children (&el->children, opts);

	GumboVector* attributes = &el->attributes;

	SCM attrvec = scm_c_make_vector (
			attributes->length,
			SCM_UNSPECIFIED);

	GUMBO_FOR_EACH (GumboAttribute, attr, attributes,
		scm_c_vector_set_x (
				attrvec, i,
				scm_list_2 (
					scm_from_utf8_symbol (attr->name),
					scm_from_utf8_string (attr->value))));


	if (attributes->length == 0) {
		return scm_cons (tag, scm_children);
	} else {
		SCM scm_attributes =
			scm_cons (scm_from_utf8_symbol ("@"),
					scm_delq (SCM_BOOL_F, scm_vector_to_list (attrvec)));
		return scm_cons (tag,
				scm_cons (scm_attributes, scm_children));
	}
}

static SCM scm_c_concat (const char* prefix, const char* suffix) {
	size_t len = strlen(suffix);
	size_t prefix_len = strlen(prefix);
	char* str = malloc(len + prefix_len + 1);
	sprintf("%s%s", prefix, suffix);
	return scm_from_utf8_stringn(str, len + prefix_len);
}

SCM parse_tree (GumboNode* node, options* options) {

	switch (node->type) {
		case GUMBO_NODE_DOCUMENT:
			{
			GumboDocument* doc = &node->v.document;

			SCM document = scm_parse_children (&doc->children, options);
			if (doc->has_doctype) {
				SCM lst = SCM_EOL;
				lst = scm_cons(scm_from_utf8_symbol("doctype"), lst);
				SCM name = scm_string_to_symbol(scm_from_utf8_stringn(doc->name, strlen(doc->name)));
				lst = scm_cons(name, lst);
				if (*doc->public_identifier != '\0') {
					lst = scm_cons(scm_c_concat("public_identifier=", doc->public_identifier), lst);
				}
				if (*doc->system_identifier != '\0') {
					lst = scm_cons(scm_c_concat("system_identifier=", doc->system_identifier), lst);
				}

				document = scm_cons(
						scm_reverse (lst),
					   	document);
			}

			return scm_cons(
					scm_from_utf8_symbol("*TOP*"),
					document);
			}

		case GUMBO_NODE_TEMPLATE:
		case GUMBO_NODE_ELEMENT:
			return gumbo_element_to_sxml(&node->v.element, options);

		case GUMBO_NODE_CDATA:
			/* TODO possibly create explicit CDATA element */
		case GUMBO_NODE_TEXT:
			{
				const char* text = node->v.text.text;
				return scm_from_utf8_stringn (text, strlen(text));
			}

		case GUMBO_NODE_WHITESPACE:
			if (options->trim_whitespace) {
				return SCM_BOOL_F;
			} else {
				const char* text = node->v.text.text;
				return scm_from_utf8_stringn (text, strlen(text));
			}

		case GUMBO_NODE_COMMENT:
		default:
			return SCM_BOOL_F;
	}
}

void
scm_init_gumbo (void)
{
	static int initialized = 0;
	if (initialized)
		return;

#ifndef SCM_MAGIC_SNARFER
#include "guile-gumbo.x"
#endif


	initialized = 1;
}
